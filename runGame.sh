#!/bin/bash

cd $(dirname $0)


godotBin()
{
if ls /etc/*release 1> /dev/null 2>&1
then
    godot $@
else
    ../.bin/Godot_v3.4.2-stable_win64.exe $@
fi
}

while true
do
godotBin .
done

extends Node

class_name BoardState

const blueNode = preload("res://Elements/GameBoard/TokenMeshBlue.tscn")
const redNode = preload("res://Elements/GameBoard/TokenMeshRed.tscn")

class Space:
	var node:Node
	var color
	func _init(node:Node,color):
		self.node = node
		self.color = color
	func resetNull():
		self.node = null
		self.color = GameManager.SIDE.NULL

signal setSpace(value)
signal win(player)

var spaces = []
#var board:Node
const BOARD_SIZE = 6
#func _init(board):
#	self.board = board
#	reset()

enum WIN_STATE {
	ORDER_POSSIBLE,
	ORDER_WIN,
	CHAOS_WIN,
	NO_WIN
}

func _ready():
	connect("setSpace",get_parent(),"setSpace")

	spaces = []
	spaces.resize(BOARD_SIZE*BOARD_SIZE)
	for n in spaces.size():
		spaces[n] = Space.new(null,GameManager.COLOR.NULL)
#		spaces[x].resize(BOARD_SIZE)
#		for y in BOARD_SIZE:
#			spaces[x][y] = Space.new(null,GameManager.COLOR.NULL) #null# Space.new(x,y)
			
func reset():
	name = "oldBoard"
	var newBoard = load("res://Elements/GameBoard/boardState.tscn").instance()
	get_parent().add_child(newBoard)
	get_parent().boardState = newBoard
	queue_free()

func checkWin():
	var full = true
	var orderPossible = false
	for x in 6:
		for y in 6:
			if getSpace(x,y).color == GameManager.COLOR.NULL:
				full = false
			var c = checkWinFromPoint(x,y)
			match c:
				WIN_STATE.ORDER_WIN:
					return GameManager.SIDE.ORDER
				WIN_STATE.ORDER_POSSIBLE:
					orderPossible = true
	if full or orderPossible == false:
		return GameManager.SIDE.CHAOS
	return GameManager.SIDE.NULL


func checkWinFromPoint(inX,inY)->int:
#	assert(0 < inX or inX > (BOARD_SIZE-1), "X out of range:" + String(inX) )
#	assert(0 < inY or inY > (BOARD_SIZE-1), "Y out of range:" + String(inY) )
	#I don't need a verification here...
	var win = false

	var start:Space = getSpace(inX,inY)
	
	var winState = WIN_STATE.NO_WIN
	
	var rowPossibles = []
	if inX < 2:
		rowPossibles.push_back(
			[start, getSpace(inX+1,inY),getSpace(inX+2,inY),getSpace(inX+3,inY),getSpace(inX+4,inY)]
		)
		if inY < 2:
			rowPossibles.push_back(
				[start, getSpace(inX+1,inY+1),getSpace(inX+2,inY+2),getSpace(inX+3,inY+3),getSpace(inX+4,inY+4)]
			)
	if inY < 2:
		rowPossibles.push_back(
			[start, getSpace(inX,inY+1),getSpace(inX,inY+2),getSpace(inX,inY+3),getSpace(inX,inY+4)]
		)
		if inX >= 4:
			rowPossibles.push_back(
				[start, getSpace(inX-1,inY+1),getSpace(inX-2,inY+2),getSpace(inX-3,inY+3),getSpace(inX-4,inY+4)]
			)
	
	for tokenRow in rowPossibles:
		var check = checkRow(tokenRow)
		match check:
			WIN_STATE.ORDER_POSSIBLE:
				winState = WIN_STATE.ORDER_POSSIBLE
			WIN_STATE.ORDER_WIN:
				return WIN_STATE.ORDER_WIN
	return winState
	
#	#check the lower right angle win
#	#check the lower left angle win
#	if inX < 2:
#		for y in 6:
#			pass
#	if inY < 2:
#		for x in 6:
#			pass
#	for x in 2:
#		for y in 2:
#			var start = getSpace(x,y)
#			for i in range(1,5):
#				pass
#	return false
const COLOR = GameManager.COLOR

func checkRow(tokens:Array)->int:
	if	tokens[0].color != COLOR.NULL and \
		tokens[0].color == tokens[1].color and \
		tokens[0].color == tokens[2].color and \
		tokens[0].color == tokens[3].color and \
		tokens[0].color == tokens[4].color:
			#todo trigger animation
			for t in tokens:
				t.node.rpc("ping")
#				t.node.ping()
			return WIN_STATE.ORDER_WIN
	var redPossible = true
	var bluePossible = true
	for t in tokens:
		if t.color == COLOR.BLUE: #t.color is not red or null
			redPossible = false
		if t.color == COLOR.RED: #t.color is not blue or null
			bluePossible = false
	if redPossible or bluePossible:
		return WIN_STATE.ORDER_POSSIBLE
	else:
		return WIN_STATE.NO_WIN
#	var c = GameManager.COLOR
#	var pick = c.NULL
#	for t in tokens:
#		if t.color == c.NULL:
#			return WIN_STATE.NO_WIN
#		else:
#			if pick == c.NULL:
#				pick = t.color
#			elif pick == t.color:
#				pass
#			else:
#				return WIN_STATE.NO_WIN
#	return WIN_STATE.ORDER_WIN
	
#		if i.color != c.NULL:
#			if pick == c.NULL:
#				pick = i.color
#			else
#	for i in range(1,4):
#		pass
#	var color = c.NULL
#	var win = WIN_STATE.ORDER_WIN
#	for t in tokens:
#		if t.color == c.NULL:
#			win = WIN_STATE.ORDER_POSSIBLE
#		elif color == c.NULL:
#			color == t.color
#		elif color != t.color:
#			return WIN_STATE.NO_WIN
#
#	return win
func makeSpace(x:int,y:int,t:Transform,color):
	var model:PackedScene = null
	match color:
		GameManager.COLOR.RED:
			model = redNode
		GameManager.COLOR.BLUE:
			model = blueNode
		_:
			return
	var node = model.instance() as Spatial
	setSpace(x,y,Space.new(node,color))
	node.name = "%db%d" % [x,y]
	add_child(node,true)
	node.resetAllPings()
	node.color = color
	node.ping()
	AudioManager.click()
	node.global_transform = t
#	checkWin()
#	var s = {
#		node = node,
#		color = color,
#		transform = t,
#	}
#	getSpace(x,y) = s
	
#

func verifyPointRange(x:int,y:int)->bool:
	if 	0 > x or x >= BOARD_SIZE or \
		0 > y or y >= BOARD_SIZE:
			return false
	return true

var nullSpace:=Space.new(null,GameManager.SIDE.NULL)
func getSpace(x,y)->Space:
	if verifyPointRange(x,y) == false:
		nullSpace.resetNull()
		return nullSpace
	return spaces[getSpaceIndex(x,y)]

func setSpace(x,y,s:Space)->Space:
	var i = getSpaceIndex(x,y)
	spaces[i] = s
	return spaces[i]

func getSpaceIndex(x,y)->int:
	return (x * BOARD_SIZE) + y
